package com.blueraptor.rabbitmqdemo;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@Profile("send")
public class Send {
    static Logger logger = LoggerFactory.getLogger(Send.class);

    private static String QUEUE_NAME = "demo-q";

    public static void fire() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername("guest");
        factory.setPassword("guest");
        factory.setHost("thor");
        factory.setPort(5672);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);
        String message = "Welcome to RabbitMQ";
        channel.basicPublish("",QUEUE_NAME,null,message.getBytes("UTF-8"));
        logger.info("[!] Send '" + message + "'");
        channel.close();
        connection.close();
    }

}
