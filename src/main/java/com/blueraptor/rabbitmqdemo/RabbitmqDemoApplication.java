package com.blueraptor.rabbitmqdemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@SpringBootApplication
public class RabbitmqDemoApplication {

    static Logger logger = LoggerFactory.getLogger(RabbitmqDemoApplication.class);

    public static void main(String[] args) throws IOException, TimeoutException {
        SpringApplication.run(RabbitmqDemoApplication.class, args);
        Send sender = new Send();
        int j = 0;
        while (j < 100) {
            sender.fire();
            j++;
        }
    }

}
